﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NUnit.Framework;

namespace WindowsFormsApp1
{

    public partial class UserForm : Form
    {

        private ErrorProvider nameErrorProvider;

        public UserForm()
        {
            InitializeComponent();
            nameErrorProvider = new ErrorProvider();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //string test = "Marcin";
            //textBox1.Text = test;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        public void PassingDataToMain()
        {
            MessageBox.Show("Hey from Form1 class");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //initalizing external object from main file
            //PassClassToForm passClassToForm = new PassClassToForm();
            //passClassToForm.PassMethod();

            if (AcceptCheckBox.Checked)
            {
                //serialize form
                /*
                var FormFields = new List<string>();
                FormFields.Add("FirstName");
                FormFields.Add("Surname");
                FormFields.Add("dateTimePicker1");
                FormFields.Add("comboBox1");
                FormFields.Add("textBox1");

                foreach (var FormFieled in FormFields)
                {
                    Console.WriteLine("Value: {0}", FirstName.Text);
                }
                */

                if (radioButton1.Checked)
                {
                    MessageBox.Show(radioButton1.Checked.ToString());
                }
                IsFirstNameValid();
                IsSurnameValid();
                IsValidNumber();

            }
            else
            {
               nameErrorProvider.SetError(AcceptCheckBox, "You have to accept.");
            }
        }

        //Validation
        private void IsFirstNameValid()
        {
            if (FirstName.Text.Length > 0)
            {
                // Clear the error, if any, in the error provider.
                FirstNameLabelValidation.Text = "";
                FirstName.BackColor = Color.White;
                nameErrorProvider.SetError(FirstName, "");
            }
            else
            {
                FirstNameLabelValidation.Text = "This field can't be empty";
                FirstName.BackColor = Color.Red;
                nameErrorProvider.SetError(FirstName, "Name is required.");
            }
        }

        private void FirstName_Validated(object sender, EventArgs e)
        {
            IsFirstNameValid();
        }

        //Validation
        private void IsSurnameValid()
        {
            if (Surname.Text.Length > 0)
            {
                // Clear the error, if any, in the error provider.
                SurnameLabelValidation.Text = "";
                Surname.BackColor = Color.White;
                nameErrorProvider.SetError(Surname, "");
            }
            else
            {
                SurnameLabelValidation.Text = "This field can't be empty";
                Surname.BackColor = Color.Red;
                nameErrorProvider.SetError(Surname, "Name is required.");
            }
        }

        private void IsValidNumber()
        {

            //obadać temat z culture
            // Set current thread culture to en-US.
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            if (Rate.Text != "" && Convert.ToDouble(Rate.Text.Replace(".", ",")) >= 0)
            {
                    // Clear the error, if any, in the error provider.
                    RateLabelValidation.Text = "";
                    Rate.BackColor = Color.White;
                    nameErrorProvider.SetError(Rate, "");
            }
            else
            {
                RateLabelValidation.Text = "Value has to be >= 0";
                Rate.BackColor = Color.Red;
                nameErrorProvider.SetError(Rate, "Name is required.");
            }

        }

        private void Rate_Validated(object sender, EventArgs e)
        {
            IsValidNumber();
        }

        private void Surname_Validated(object sender, EventArgs e)
        {
            IsSurnameValid();
        }


        private void checkboxValidation(object sender, EventArgs e)
        {
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (AcceptCheckBox.Checked)
            {
                nameErrorProvider.SetError(AcceptCheckBox, "");
            }
            else
            {
                nameErrorProvider.SetError(AcceptCheckBox, "You have to accept.");
            }
        }

        private void Surname_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextAreaLabel_Click(object sender, EventArgs e)
        {

        }

        private void form1ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void formToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        Form2 secondForm = new Form2();
        private void sQLToolStripMenuItem_Click(object sender, EventArgs e)
        {  
            secondForm.Show();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
