﻿using NUnit.Framework;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{

    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;


        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "csharpmysql";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + 
		    database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }


        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Select statement
        public List<string>[] Select()
        {
            string query = "SELECT * FROM `csharpmysql`.`table`;";

            //Create a list to store the result
            List<string>[] list = new List<string>[2];
            //list[0] = new List<string>();
            //list[1] = new List<string>();

            //Open connection
            if (OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                // Always call Read before accessing data.
                while (dataReader.Read())
                {
                    Console.WriteLine(dataReader.GetString(0) + ", " + dataReader.GetString(1));
                }

                //Read the data and store them in the list
                /*
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["name"] + "");
                }
                */



                //close Data Reader
                dataReader.Close();

                //close Connection
                CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }


        //Update statement
        public void Update()
        {
            string query = "UPDATE `table` SET `name` = 'Marcin' WHERE `table`.`id` = 1;";

            //Open connection
            if (OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                CloseConnection();
            }
        }
    }


    class Count
    {
        public double Add(double a, double b)
        {
            double c;
            return c = a + b;
        }
    }

    class PassClassToForm
    {
        public void PassMethod()
        {
            Console.WriteLine("I came from external Class included in main file.");
        }
    }
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            /*
            string[] values = { "-17.455",
                         "190.34001", "5,2"};
            double result;
            foreach (string value in values)
            {
                try
                {
                    result = Convert.ToDouble(value);
                    Console.WriteLine("Converted '{0}' to {1}.", value, result);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to convert '{0}' to a Double.", value);
                }
                catch (OverflowException)
                {
                    Console.WriteLine("'{0}' is outside the range of a Double.", value);
                }
            }
            */

            DBConnect ConnectMe = new DBConnect();
            ConnectMe.Select();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            UserForm runAutomate = new UserForm();
            //runAutomate.PassingDataToMain();

            Application.Run(new UserForm());



            //string crashString = "1,2";
            //Console.WriteLine(Convert.ToDouble(crashString));
        }
    }
}
